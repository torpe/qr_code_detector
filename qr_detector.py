import numpy as np
import cv2
import math
from pyzbar import pyzbar
import os
import pathlib

def detect_qr_code(image, num_of_bar_codes=2):
    barcodes = pyzbar.decode(image)
    if len(barcodes) >= num_of_bar_codes:
        b_corners = []
        for barcode in barcodes:
            b_corners.append([])
            points_x = []
            points_y = []
            for p in barcode.polygon:
                points_x.append(p.x)
                points_y.append(p.y)
            i = np.argsort(points_y)
            if (points_x[i[0]] < points_x[i[1]]):
                b_corners[-1].append([points_x[i[0]], points_y[i[0]]])
                b_corners[-1].append([points_x[i[1]], points_y[i[1]]])
            else:
                b_corners[-1].append([points_x[i[1]], points_y[i[1]]])
                b_corners[-1].append([points_x[i[0]], points_y[i[0]]])

            if (points_x[i[2]] > points_x[i[3]]):
                b_corners[-1].append([points_x[i[2]], points_y[i[2]]])
                b_corners[-1].append([points_x[i[3]], points_y[i[3]]])
            else:
                b_corners[-1].append([points_x[i[3]], points_y[i[3]]])
                b_corners[-1].append([points_x[i[2]], points_y[i[2]]])
        for i in range(0,num_of_bar_codes):
            for j in range(i+1, num_of_bar_codes):
                if (b_corners[i][0][1] > b_corners[j][0][1]):
                    b_corners[i], b_corners[j] = b_corners[j], b_corners[i]
        if (b_corners[0][0][0] > b_corners[1][0][0]):
            b_corners[1], b_corners[0] = b_corners[0], b_corners[1]
        return b_corners
    else:
        raise Exception("Not enough qr code detected")

def convert_and_treshold_image(image_file_name, treshold=175, debug=True, debug_dir="."):
    image = cv2.imread(image_file_name)
    file_path = pathlib.Path(image_file_name)
    file_name = file_path.name
    file_extension = file_path.suffix
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    if debug is not None:
        cv2.imwrite(os.path.join(debug_dir, file_name.replace(file_extension, "_g.JPG")), gray)
    tresh = cv2.threshold(gray, treshold, 255, cv2.THRESH_BINARY)[1]
    if debug is not None:
        cv2.imwrite(os.path.join(debug_dir, file_name.replace(file_extension, "_t.JPG")), tresh)
    return image, tresh

def generate_points_for_qr_codes(b_corners, table_shape=(760, 2310), qr_code_shape=(110,110)):
    points_left = np.array([[b_corners[0][0][0], b_corners[0][0][1]], [b_corners[0][1][0], b_corners[0][1][1]],
                            [b_corners[0][2][0], b_corners[0][2][1]], [b_corners[0][3][0], b_corners[0][3][1]]],
                           np.int32)
    points_left_im = np.array([[0, 0], [qr_code_shape[0]-1, 0], [qr_code_shape[0]-1, qr_code_shape[1]-1], [0, qr_code_shape[1]-1]], np.int32)
    points_right = np.array([[b_corners[1][0][0], b_corners[1][0][1]], [b_corners[1][1][0], b_corners[1][1][1]],
                             [b_corners[1][2][0], b_corners[1][2][1]], [b_corners[1][3][0], b_corners[1][3][1]]],
                            np.int32)
    points_right_im = np.array([[table_shape[0]-qr_code_shape[0], 0], [table_shape[0]-1, 0], [table_shape[0]-1, qr_code_shape[1]-1], [table_shape[0]-qr_code_shape[0], qr_code_shape[1]-1]], np.int32)


    points = np.append(points_left, points_right, axis=0).astype(np.float32)
    points_im = np.append(points_left_im, points_right_im, axis=0).astype(np.float32)

    if len(b_corners) == 3:
        points_mid = np.array([[b_corners[2][0][0], b_corners[2][0][1]], [b_corners[2][1][0], b_corners[2][1][1]],
                             [b_corners[2][2][0], b_corners[2][2][1]], [b_corners[2][3][0], b_corners[2][3][1]]],
                            np.int32)
        points_mid_im = np.array([[table_shape[0] / 2 - qr_code_shape[0] / 2, 3 * qr_code_shape[1]], [table_shape[0] / 2 + qr_code_shape[0] / 2 - 1, 3 * qr_code_shape[1]], [table_shape[0] / 2 + qr_code_shape[0] / 2 - 1, 4 * qr_code_shape[1] - 1], [table_shape[0] / 2 - qr_code_shape[0] / 2, 4 * qr_code_shape[1] - 1]], np.int32)
        points = np.append(points, points_mid, axis=0).astype(np.float32)
        points_im = np.append(points_im, points_mid_im, axis=0).astype(np.float32)
    return points, points_im

def transform_and_cut_table(image, M, debug=True, table_shape=(760, 2310), qr_code_shape=(110,110), debug_dir=".", original_file_name=None):
    finalimage = cv2.warpPerspective(image, M, table_shape)
    if debug:
        file_path = pathlib.Path(original_file_name)
        file_name = file_path.name
        file_extension = file_path.suffix
        cv2.imwrite(os.path.join(debug_dir, file_name.replace(file_extension, "_res2.JPG")), finalimage)
        table_polygon = [np.array([[qr_code_shape[0] - 1, qr_code_shape[1] - 1], [qr_code_shape[0] - 1, table_shape[1] - 1],
                   [table_shape[0] - qr_code_shape[0], table_shape[1] - 1],
                   [table_shape[0] - qr_code_shape[0], qr_code_shape[1] - 1]])]
        cv2.polylines(finalimage, table_polygon, True, (36, 255, 12), thickness=3)
        image_t = cv2.warpPerspective(finalimage, M, (image.shape[1], image.shape[0]), flags=cv2.WARP_INVERSE_MAP,
                                  borderMode=cv2.BORDER_CONSTANT)
        image_tg = cv2.cvtColor(image_t, cv2.COLOR_BGR2GRAY)
        image[image_tg != 0] = image[image_tg != 0] * 0
        # cv2.polylines(image, [cut_points], True, (36,255,12), thickness=3)
        cv2.imwrite(os.path.join(out_dir, file_name.replace(file_extension, "_cut2.JPG")), image + image_t)
    return finalimage


if __name__ == "__main__":
    in_dir = "/home/torpe/dev/edvard_data/selfmade_test_data_3qrcodes/"
    out_dir = "/home/torpe/dev/edvard_data/selfmade_test_data_3qrcodes_out/"
    os.makedirs(out_dir, exist_ok=True)

    in_files = [
        "qr_test_0.jpg",
        "qr_test_1.jpg",
        "qr_test_2.jpg",
        "qr_test_3.jpg",
        "qr_test_4.jpg",
        "qr_test_5.jpg",
        "qr_test_6.jpg"
    ]
    

    for f in in_files:
        try:
            image, tresh = convert_and_treshold_image(os.path.join(in_dir, f), 127, True, out_dir)
            b_corners = detect_qr_code(tresh, 3)
            qr_code_shape = (100, 100)
            table_shape = (700, 2100)
            points, points_im = generate_points_for_qr_codes(b_corners, table_shape, qr_code_shape)

            M, mask = cv2.findHomography(points, points_im)
            finalimage = transform_and_cut_table(image, M, True, table_shape, qr_code_shape, out_dir, f)
        except Exception as e:
            print(f)
            print(e)



